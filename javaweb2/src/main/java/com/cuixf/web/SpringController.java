package com.cuixf.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("/cuixf")
public class SpringController {
	
	@RequestMapping("/{page}")
	public String index(@PathVariable String page,String name,Model model){
		System.out.println("名字为："+name);
		model.addAttribute("name", name);
		return page;
	}

}
